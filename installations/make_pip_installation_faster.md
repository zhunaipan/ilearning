## Step1: Select a mirror site in China below.
- aliyun: https://mirrors.aliyun.com/pypi/simple/
- ustc: https://pypi.mirrors.ustc.edu.cn/simple/
- douban: https://pypi.douban.com/simple/
- tsinghua: https://pypi.tuna.tsinghua.edu.cn/simple/

## Step2: Install the package, such as `cmake`.

-  **Install `cmake` with the mirror site for temporary.** 
```
pip install cmake -i https://mirrors.aliyun.com/pypi/simple/
```
-  **Configure the pip.conf, then install `cmake`.** 
```
mkdir ~/.pip
echo -e '[global]\nindex-url = https://mirrors.aliyun.com/pypi/simple/' > ~/.pip/pip.conf
pip install cmake
```

 :fa-bullhorn:  _when you execute the command: `cat ~/.pip/pip.conf`, the below info will be shown._ 
```
[global]
index-url = https://mirrors.aliyun.com/pypi/simple/
```


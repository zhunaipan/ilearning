## Step1: Install the `docker`, if already installed, skip the step.
```
pip install docker
```
## Step2: Search the `ubuntu` images.
```
docker search ubuntu
```
```
# the console output.
NAME                                                      DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
ubuntu                                                    Ubuntu is a Debian-based Linux operating sys…   10680               [OK]                
dorowu/ubuntu-desktop-lxde-vnc                            Docker image to provide HTML5 VNC interface …   411                                     [OK]
rastasheep/ubuntu-sshd                                    Dockerized SSH service, built on top of offi…   245                                     [OK]
consol/ubuntu-xfce-vnc                                    Ubuntu container with "headless" VNC session…   212                                     [OK]
ubuntu-upstart                                            Upstart is an event-based replacement for th…   107                 [OK]                
ansible/ubuntu14.04-ansible                               Ubuntu 14.04 LTS with ansible                   98                                      [OK]
neurodebian                                               NeuroDebian provides neuroscience research s…   67                  [OK]                
1and1internet/ubuntu-16-nginx-php-phpmyadmin-mysql-5      ubuntu-16-nginx-php-phpmyadmin-mysql-5          50                                      [OK]
ubuntu-debootstrap                                        debootstrap --variant=minbase --components=m…   43                  [OK]                
nuagebec/ubuntu                                           Simple always updated Ubuntu docker images w…   24                                      [OK]
i386/ubuntu                                               Ubuntu is a Debian-based Linux operating sys…   19                                      
1and1internet/ubuntu-16-apache-php-5.6                    ubuntu-16-apache-php-5.6                        14                                      [OK]
1and1internet/ubuntu-16-apache-php-7.0                    ubuntu-16-apache-php-7.0                        13                                      [OK]
ppc64le/ubuntu                                            Ubuntu is a Debian-based Linux operating sys…   13                                      
1and1internet/ubuntu-16-nginx-php-phpmyadmin-mariadb-10   ubuntu-16-nginx-php-phpmyadmin-mariadb-10       11                                      [OK]
1and1internet/ubuntu-16-nginx-php-5.6-wordpress-4         ubuntu-16-nginx-php-5.6-wordpress-4             7                                       [OK]
1and1internet/ubuntu-16-apache-php-7.1                    ubuntu-16-apache-php-7.1                        6                                       [OK]
darksheer/ubuntu                                          Base Ubuntu Image -- Updated hourly             5                                       [OK]
pivotaldata/ubuntu                                        A quick freshening-up of the base Ubuntu doc…   4                                       
1and1internet/ubuntu-16-nginx-php-7.0                     ubuntu-16-nginx-php-7.0                         4                                       [OK]
pivotaldata/ubuntu16.04-build                             Ubuntu 16.04 image for GPDB compilation         2                                       
1and1internet/ubuntu-16-php-7.1                           ubuntu-16-php-7.1                               1                                       [OK]
1and1internet/ubuntu-16-sshd                              ubuntu-16-sshd                                  1                                       [OK]
smartentry/ubuntu                                         ubuntu with smartentry                          1                                       [OK]
pivotaldata/ubuntu-gpdb-dev                               Ubuntu images for GPDB development              1               
```
## Step3: Pull the `ubuntu` image
```
docker pull ubuntu
```
## Step4: Launch the docker container.
- Foreground mode
```
docker run -it ubuntu bash
```
- Foreground mode with alias name
```
docker run --name test -it ubuntu bash
```
- Foreground mode with directories mounted.
```
# the source directory on the host: /Users/jackson/workspace/
# the target directory mounted in the container.

docker run -it --privileged -v /Users/jackson/workspace/:/home/ ubuntu bash
```

 :fa-bullhorn: _If you want to relaunch the `ubuntu` container after exit._
```
# query the container ID.
docker ps
``` 
```
# the console output
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
81cd18b9d4dd        ubuntu              "bash"              18 minutes ago      Up 39 seconds                           focused_bardeen
```

```
# 81cd18b9d4dd is the container ID.
docker exec -it 81cd18b9d4dd bash
```